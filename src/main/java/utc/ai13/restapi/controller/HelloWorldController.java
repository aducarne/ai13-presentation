package utc.ai13.restapi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/hello")
public class HelloWorldController {
    @GetMapping
    @ResponseBody
    public String sayHello() {
        return "Hello World";
    }

    @GetMapping("/{nb}")
    @ResponseBody
    public String sayHelloXTimes(@PathVariable Integer nb) {
        return IntStream.rangeClosed(1, nb)
                .mapToObj(i -> "Hello World " + i)
                .collect(Collectors.joining("\n"));
    }

    @GetMapping(params = "nb")
    @ResponseBody
    public String sayHelloXTimesParam(@RequestParam Integer nb) {
        return IntStream.rangeClosed(1, nb)
                .mapToObj(i -> "Hello World " + i)
                .collect(Collectors.joining("\n"));
    }

//    @GetMapping
//    @ResponseBody
//    public String sayHelloXTimesBody(@RequestBody Integer nb) {
//        return IntStream.rangeClosed(1, nb)
//                .mapToObj(i -> "Hello World " + i)
//                .collect(Collectors.joining("\n"));
//    }

//    @GetMapping
//    public String sayHelloXTimesBodyObj(@RequestBody Container container) {
//        return IntStream.rangeClosed(1, container.getNb())
//                .mapToObj(i -> "Hello World " + i)
//                .collect(Collectors.joining("\n"));
//    }
}
